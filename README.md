# Rollbar for Bitbucket Pipelines

This repo shows how to get deploy notifications in [Rollbar](https://rollbar.com) when builds succeed in [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines).

## Integration Instructions

1. Add the `ROLLBAR_ACCESS_TOKEN` environment variable, as described below.
2. Copy [`rollbar_record_deploy.py`](https://bitbucket.org/rollbar/rollbar-bitbucket-pipelines/raw/master/rollbar_record_deploy.py) to your project.
3. Add the step `python rollbar_record_deploy.py` to your `bitbucket-pipelines.yml`. For an example, see [`bitbucket-pipelines.yml`](https://bitbucket.org/rollbar/rollbar-bitbucket-pipelines/src/2b50df8ffe15aa9995ecb1c89542086d62cb4b6b/bitbucket-pipelines.yml?at=master&fileviewer=file-view-default#bitbucket-pipelines.yml-11) in this repo.

## Environment Variables

The following environment variables are supported. They can be configured in Bitbucket > Settings > Environment Variables. Only `ROLLBAR_ACCESS_TOKEN` is required.

|Name|Required|Secured|Description|
|----|--------|-------|-----------|
|`ROLLBAR_ACCESS_TOKEN`|Required|Secured|a post_server_item access token for your Rollbar project. Go to your project in Rollbar, then Settings > Project Access Tokens.|
|`ROLLBAR_ENVIRONMENT_{branch}`|Optional|Not secured|the name of the rollbar environment where we should record deploys for the branch {branch}. See examples below.|
|`ROLLBAR_ENDPOINT`|Optional|Not secured|The URL to the deploy endpoint. Most users will not need to set this. Default value is https://api.rollbar.com/api/1/deploy/|

By default, only builds from the 'master' branch will be reported, and they will be reported to the 'production' environment. This can be customized by setting environment variables of the form:

```
ROLLBAR_ENVIRONMENT_{branch}={rollbar_environment_name}
```

For example, if your master branch deploys to 'prod' and your 'develop' branch deploys to 'staging':

```
ROLLBAR_ENVIRONMENT_master=prod
ROLLBAR_ENVIRONMENT_develop=staging
```

To disable reporting for the master branch, set it to an empty value:

```
ROLLBAR_ENVIRONMENT_master=
```
